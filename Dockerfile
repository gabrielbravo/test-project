# Dockerfile for an image with a simulated high vulnerability
FROM alpine:3.14.0

# Copy the HTML file to the container
COPY index.html /app/

# Expose port 80
EXPOSE 80

# Command to run when the container starts
CMD ["sh", "-c", "cat /app/index.html && sleep infinity"]
